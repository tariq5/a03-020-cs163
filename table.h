//tariq khandwalla
//A03 CS163
//Friday May 10, 2024
//
#include <cstring>
#include <iostream>
#include <fstream>

using namespace std;

#ifndef TABLE_H
#define TABLE_H

#define MAX_NAME_LENGTH 50
#define MAX_DESCRIPTION_LENGTH 100
#define MAX_ASPECTS_LENGTH 3
#define MAX_ADDITIONAL_INFO_LENGTH 50

struct Keyword {
  char name[MAX_NAME_LENGTH];
  char description[MAX_DESCRIPTION_LENGTH];
  char aspects[MAX_ASPECTS_LENGTH][MAX_NAME_LENGTH];
  int searchHistory;
  char additionalInfo[MAX_ADDITIONAL_INFO_LENGTH];
};

struct HashEntry {
  Keyword* keyword;
  HashEntry* next;
};

class Table {
private:
  HashEntry** hashTable;
  int tableSize;

  int hashFunction(const char* keywordName) const {
    int sum = 0;
    for (int i = 0; i < strlen(keywordName); i++) {
      sum += keywordName[i];
    }
    return sum % tableSize;
  }

public:
  Table(int size) {
    tableSize = size;
    hashTable = new HashEntry*[tableSize];
    for (int i = 0; i < tableSize; i++) {
      hashTable[i] = nullptr;
    }
  }

  ~Table() {
    for (int i = 0; i < tableSize; i++) {
      HashEntry* current = hashTable[i];
      while (current != nullptr) {
        HashEntry* next = current->next;
        delete current;
        current = next;
      }
    }
    delete[] hashTable;
  }

  void addKeyword(const char* name, const char* description, const char* aspects[], int searchHistory, const char* additionalInfo) {
    int index = hashFunction(name);
    HashEntry* entry = new HashEntry;
    entry->keyword = new Keyword;
    strcpy(entry->keyword->name, name);
    strcpy(entry->keyword->description, description);
    for (int i = 0; i < MAX_ASPECTS_LENGTH; i++) {
      strcpy(entry->keyword->aspects[i], aspects[i]);
    }
    entry->keyword->searchHistory = searchHistory;
    strcpy(entry->keyword->additionalInfo, additionalInfo);
    entry->next = hashTable[index];
    hashTable[index] = entry;
  }

  void displayKeywords() const {
    for (int i = 0; i < tableSize; i++) {
      HashEntry* current = hashTable[i];
      while (current != nullptr) {
        cout << "Keyword: " << current->keyword->name << endl;
        cout << "Description: " << current->keyword->description << endl;
        for (int j = 0; j < MAX_ASPECTS_LENGTH; j++) {
          cout << "Aspect " << (j + 1) << ": " << current->keyword->aspects[j] << endl;
        }
        cout << "Search History: " << current->keyword->searchHistory << endl;
        cout << "Additional Info: " << current->keyword->additionalInfo << endl << endl;
        current = current->next;
      }
    }
  }

  void removeKeyword(const char* name) {
    int index = hashFunction(name);
    HashEntry* current = hashTable[index];
    HashEntry* previous = nullptr;
    while (current != nullptr && strcmp(current->keyword->name, name) != 0) {
      previous = current;
      current = current->next;
    }
    if (current != nullptr) {
      if (previous == nullptr) {
        hashTable[index] = current->next;
      } else {
        previous->next = current->next;
      }
      delete current;
    }
  }

  Keyword* retrieveKeyword(const char* name) const {
    int index = hashFunction(name);
    HashEntry* current = hashTable[index];
    while (current != nullptr && strcmp(current->keyword->name, name) != 0) {
      current = current->next;
    }
    if (current != nullptr) {
      return current->keyword;
    } else {
      return nullptr;
    }
  }
};

#endif // TABLE_H
